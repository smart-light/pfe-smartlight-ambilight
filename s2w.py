import os
import time

from api import SmartLight

try:
    SmartLight('admin', '324c0f8e73cb16614fed0add7792ac958d5d3162428ca21b359015f48dbb522e').login()
except Exception as e:
    print "Core injoignable\n %s" % e
    
groupLamp = 'TV'

def traitement(pic):
    if 'ff00000f' == pic[0:8]:
        colors = {
            'csum': pic[8:14],
            'left': pic[14:20],
            'right': pic[20:26],
            'top': pic[26:32],
            'bottom': pic[32:38]
        }
        try:
            SmartLight.getInstance().call(groupLamp, colors)
        except Exception:
            return

master, slave = os.openpty()

print "Configure atmolight plugin to send to %s" % os.ttyname(slave)
stdout = os.fdopen(master, 'r', 0)

old_time = time.time()

while True:
    pic = stdout.read(19).encode('hex')
    new_time = time.time()
    if new_time - old_time >= 0.25:
        traitement(pic)
        old_time = new_time
